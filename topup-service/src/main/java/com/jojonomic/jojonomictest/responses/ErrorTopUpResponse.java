package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorTopUpResponse {
    @JsonProperty(value = "error")
    private boolean error;

    @JsonProperty(value = "reff_id")
    private String reffId;

    @JsonProperty(value = "message")
    private String message;

    @JsonCreator
    public ErrorTopUpResponse(String reffId, String message) {
        this.error = true;
        this.reffId = reffId;
        this.message = message;
    }
}
