package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SuccessTopUpResponse {
    @JsonProperty(value = "error")
    private boolean error;

    @JsonProperty(value = "reff_id")
    private String reffId;

    @JsonCreator
    public SuccessTopUpResponse(String reffId) {
        this.error = false;
        this.reffId = reffId;
    }
}
