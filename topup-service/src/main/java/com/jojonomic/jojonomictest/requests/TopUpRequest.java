package com.jojonomic.jojonomictest.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TopUpRequest {

    @JsonProperty(value = "gram")
    private String gram;

    @JsonProperty(value = "harga")
    private String harga;

    @JsonProperty(value = "norek")
    private String noRek;

    @JsonCreator
    public TopUpRequest() {

    }

    public String getGram() {
        return gram;
    }

    public String getHarga() {
        return harga;
    }

    public String getNoRek() {
        return noRek;
    }
}
