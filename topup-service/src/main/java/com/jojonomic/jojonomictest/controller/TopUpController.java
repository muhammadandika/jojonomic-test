package com.jojonomic.jojonomictest.controller;

import com.jojonomic.jojonomictest.entity.GoldPrice;
import com.jojonomic.jojonomictest.repository.GoldPriceRepository;
import com.jojonomic.jojonomictest.requests.TopUpRequest;
import com.jojonomic.jojonomictest.responses.ErrorTopUpResponse;
import com.jojonomic.jojonomictest.responses.SuccessTopUpResponse;
import com.jojonomic.jojonomictest.utils.TopUpMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class TopUpController {

    @Autowired
    KafkaTemplate<String, TopUpMessage> kafkaTemplate;

    @Autowired
    GoldPriceRepository goldPriceRepository;

    @PostMapping("/topup")
    public ResponseEntity topUp(@RequestBody TopUpRequest request) {
        Double topUpPrice = 0.0;
        Double goldWeight = 0.0;

        try {
            topUpPrice = Double.parseDouble(request.getHarga());
            goldWeight = Double.parseDouble(request.getGram());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (goldWeight < 0.001) {
            return ResponseEntity.ok(new ErrorTopUpResponse(null, "Minimal pembelian 0.001 gram"));
        }

        if (goldWeight % 0.001 != 0) {
            return ResponseEntity.ok(new ErrorTopUpResponse(null, "Pembelian harus kelipatan 0.001"));
        }

        List<GoldPrice> listGoldPrice = goldPriceRepository.findAll(Sort.by(Sort.Direction.DESC, "createdAt"));

        if (listGoldPrice.size() < 1) {
            return ResponseEntity.ok(new ErrorTopUpResponse(null, "Harga emas saat ini tidak ditemukan"));
        }

        GoldPrice goldPrice = listGoldPrice.get(0);

        if (topUpPrice < goldPrice.getTopUpPrice()) {
            return ResponseEntity.ok(new ErrorTopUpResponse(Long.toString(goldPrice.getReffId()), "Harga tidak boleh kurang dari harga saat ini"));
        }

        TopUpMessage message = new TopUpMessage();
        message.setGram(request.getGram());
        message.setHarga(request.getHarga());
        message.setNoRek(request.getNoRek());

        kafkaTemplate.send("top-up", message);

        return ResponseEntity.ok(new SuccessTopUpResponse(Long.toString(goldPrice.getReffId())));
    }
}
