package com.jojonomic.jojonomictest;

import com.jojonomic.jojonomictest.entity.GoldPrice;
import com.jojonomic.jojonomictest.repository.InputPriceRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class InputPriceStorageApplication {

	private InputPriceRepository inputPriceRepository;

	public InputPriceStorageApplication(InputPriceRepository inputPriceRepository) {
		this.inputPriceRepository = inputPriceRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(InputPriceStorageApplication.class, args);
	}

	@KafkaListener(topics = "input-harga", groupId = "e_gold", containerFactory = "goldPriceListener")
	public void onEvent(GoldPrice goldPrice) {
		inputPriceRepository.save(goldPrice);
	}
}
