package com.jojonomic.jojonomictest.repository;

import com.jojonomic.jojonomictest.entity.GoldPrice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InputPriceRepository extends JpaRepository<GoldPrice, Long> {

}
