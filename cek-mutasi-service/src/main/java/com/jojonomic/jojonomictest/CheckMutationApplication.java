package com.jojonomic.jojonomictest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckMutationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckMutationApplication.class, args);
	}
}
