package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionResponse {
    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "gram")
    private Double goldWeight;

    @JsonProperty(value = "harga_topup")
    private Double hargaTopup;

    @JsonProperty(value = "harga_buyback")
    private Double hargaBuyBack;

    @JsonProperty(value = "saldo")
    private Double goldBalance;

    @JsonProperty(value = "date")
    private long createdAt;

    public TransactionResponse() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getGoldWeight() {
        return goldWeight;
    }

    public void setGoldWeight(Double goldWeight) {
        this.goldWeight = goldWeight;
    }

    public Double getHargaTopup() {
        return hargaTopup;
    }

    public void setHargaTopup(Double hargaTopup) {
        this.hargaTopup = hargaTopup;
    }

    public Double getHargaBuyBack() {
        return hargaBuyBack;
    }

    public void setHargaBuyBack(Double hargaBuyBack) {
        this.hargaBuyBack = hargaBuyBack;
    }

    public Double getGoldBalance() {
        return goldBalance;
    }

    public void setGoldBalance(Double goldBalance) {
        this.goldBalance = goldBalance;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}
