package com.jojonomic.jojonomictest.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckMutationRequest {
    @JsonProperty(value = "norek")
    private String noRek;

    @JsonProperty(value = "start_date")
    private Long startDate;

    @JsonProperty(value = "end_date")
    private Long endDate;

    @JsonCreator
    public CheckMutationRequest() {

    }

    public String getNoRek() {
        return noRek;
    }

    public Long getStartDate() {
        return startDate;
    }

    public Long getEndDate() {
        return endDate;
    }
}
