package com.jojonomic.jojonomictest.controller;

import com.jojonomic.jojonomictest.entity.Transaction;
import com.jojonomic.jojonomictest.repository.TransactionRepository;
import com.jojonomic.jojonomictest.requests.CheckMutationRequest;
import com.jojonomic.jojonomictest.responses.SuccessResponse;
import com.jojonomic.jojonomictest.responses.TransactionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionRepository;

    @PostMapping("/mutasi")
    ResponseEntity<SuccessResponse<List<TransactionResponse>>> mutation(@RequestBody CheckMutationRequest request) {
        List<Transaction> transactions = transactionRepository.findAllByNoRekAndCreatedAtAfterAndCreatedAtBefore(
                request.getNoRek(),
                request.getStartDate(),
                request.getEndDate()
        );

        if (transactions.size() < 1) {
            return ResponseEntity.ok(new SuccessResponse<>(null));
        }

        List<TransactionResponse> transactionResponses = transactions.stream().map(
                transaction -> {
                    TransactionResponse transactionResponse = new TransactionResponse();
                    transactionResponse.setCreatedAt(transaction.getCreatedAt());
                    transactionResponse.setType(transaction.getType());
                    transactionResponse.setGoldWeight(transaction.getGoldWeight());
                    transactionResponse.setHargaTopup(transaction.getHargaTopup());
                    transactionResponse.setHargaBuyBack(transaction.getHargaBuyBack());
                    transactionResponse.setGoldBalance(transaction.getGoldBalance());

                    return transactionResponse;
                }
        ).collect(Collectors.toList());

        return ResponseEntity.ok(new SuccessResponse<>(transactionResponses));
    }
}
