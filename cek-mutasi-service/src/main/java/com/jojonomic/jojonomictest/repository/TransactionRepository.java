package com.jojonomic.jojonomictest.repository;

import com.jojonomic.jojonomictest.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByNoRekAndCreatedAtAfterAndCreatedAtBefore(String norek, Long startDate, Long endDate);
}
