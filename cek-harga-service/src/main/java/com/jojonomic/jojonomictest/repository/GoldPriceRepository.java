package com.jojonomic.jojonomictest.repository;

import com.jojonomic.jojonomictest.entity.GoldPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoldPriceRepository extends JpaRepository<GoldPrice, Long> {

}
