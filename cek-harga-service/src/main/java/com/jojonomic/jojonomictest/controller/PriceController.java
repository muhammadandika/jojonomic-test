package com.jojonomic.jojonomictest.controller;

import com.jojonomic.jojonomictest.entity.GoldPrice;
import com.jojonomic.jojonomictest.entity.Response;
import com.jojonomic.jojonomictest.repository.GoldPriceRepository;
import com.jojonomic.jojonomictest.responses.GoldPriceResponse;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class PriceController {
    private GoldPriceRepository repository;

    public PriceController(GoldPriceRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/check-harga")
    public ResponseEntity<Response<GoldPriceResponse>> getGoldPrice() {
        List<GoldPrice> listGoldPrice = repository.findAll(Sort.by(Sort.Direction.DESC, "createdAt"));

        if (listGoldPrice.isEmpty()) {
            return new ResponseEntity<>(new Response(null), HttpStatus.OK);
        }

        GoldPrice goldPrice = listGoldPrice.get(0);

        GoldPriceResponse goldPriceResponse = new GoldPriceResponse(
                goldPrice.getBuyBackPrice(),
                goldPrice.getTopUpPrice()
        );

        return new ResponseEntity<>(new Response(goldPriceResponse), HttpStatus.OK);
    }
}
