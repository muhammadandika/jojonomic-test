package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GoldPriceResponse {
    @JsonProperty(value = "harga_buyback")
    private Double buyBackPrice;

    @JsonProperty(value = "harga_topup")
    private Double topUpPrice;

    @JsonCreator
    public GoldPriceResponse(Double buyBackPrice, Double topUpPrice) {
        this.buyBackPrice = buyBackPrice;
        this.topUpPrice = topUpPrice;
    }
}

