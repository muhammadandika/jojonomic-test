package com.jojonomic.jojonomictest.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Response<T> {
    @JsonProperty
    private boolean error;

    @JsonProperty
    private T data;

    public Response(T data) {
        this.error = false;
        this.data = data;
    }
}
