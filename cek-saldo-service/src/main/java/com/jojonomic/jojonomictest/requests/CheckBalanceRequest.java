package com.jojonomic.jojonomictest.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckBalanceRequest {
    @JsonProperty(value = "norek")
    private String noRek;

    CheckBalanceRequest() {}

    public String getNoRek() {
        return noRek;
    }
}
