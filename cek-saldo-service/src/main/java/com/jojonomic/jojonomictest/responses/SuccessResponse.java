package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SuccessResponse<T> {
    @JsonProperty(value = "error")
    private boolean error;

    @JsonProperty(value = "data")
    private T data;

    @JsonCreator
    public SuccessResponse(T data) {
        this.error = false;
        this.data = data;
    }
}
