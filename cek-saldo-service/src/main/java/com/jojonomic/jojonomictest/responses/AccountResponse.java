package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountResponse {
    @JsonProperty(value = "norek")
    private String noRek;

    @JsonProperty(value = "saldo")
    private Double saldo;

    public AccountResponse(String noRek, Double saldo) {
        this.noRek = noRek;
        this.saldo = saldo;
    }
}
