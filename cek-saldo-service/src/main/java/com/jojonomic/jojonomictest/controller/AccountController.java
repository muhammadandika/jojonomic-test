package com.jojonomic.jojonomictest.controller;

import com.jojonomic.jojonomictest.entity.Account;
import com.jojonomic.jojonomictest.repository.AccountRepository;
import com.jojonomic.jojonomictest.requests.CheckBalanceRequest;
import com.jojonomic.jojonomictest.responses.AccountResponse;
import com.jojonomic.jojonomictest.responses.ErrorResponse;
import com.jojonomic.jojonomictest.responses.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @PostMapping("/check-saldo")
    public ResponseEntity checkSaldo(@RequestBody CheckBalanceRequest request) {
        Account account = accountRepository.findByAccountNumber(request.getNoRek());

        if (account == null) {
            return ResponseEntity.ok(new ErrorResponse(null, "Akun rekening anda tidak ditemukan"));
        }

        AccountResponse accountResponse = new AccountResponse(account.getAccountNumber(), account.getBalance());

        return ResponseEntity.ok(new SuccessResponse(accountResponse));
    }
}
