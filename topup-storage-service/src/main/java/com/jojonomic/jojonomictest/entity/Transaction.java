package com.jojonomic.jojonomictest.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tbl_transaksi")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "reff_id")
    private Long reffId;

    @Column(name = "norek")
    private String noRek;

    @Column(name = "type")
    private String type;

    @Column(name = "gold_weight")
    private Double goldWeight;

    @Column(name = "harga_topup")
    private Double hargaTopup;

    @Column(name = "harga_buyback")
    private Double hargaBuyBack;

    @Column(name = "gold_balance")
    private Double goldBalance;

    @Column(name = "created_at")
    private long createdAt;

    public Transaction() {

    }

    public Long getReffId() {
        return reffId;
    }

    public void setReffId(Long reffId) {
        this.reffId = reffId;
    }

    public String getNoRek() {
        return noRek;
    }

    public void setNoRek(String noRek) {
        this.noRek = noRek;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getGoldWeight() {
        return goldWeight;
    }

    public void setGoldWeight(Double goldWeight) {
        this.goldWeight = goldWeight;
    }

    public Double getHargaTopup() {
        return hargaTopup;
    }

    public void setHargaTopup(Double hargaTopup) {
        this.hargaTopup = hargaTopup;
    }

    public Double getHargaBuyBack() {
        return hargaBuyBack;
    }

    public void setHargaBuyBack(Double hargaBuyBack) {
        this.hargaBuyBack = hargaBuyBack;
    }

    public Double getGoldBalance() {
        return goldBalance;
    }

    public void setGoldBalance(Double goldBalance) {
        this.goldBalance = goldBalance;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}
