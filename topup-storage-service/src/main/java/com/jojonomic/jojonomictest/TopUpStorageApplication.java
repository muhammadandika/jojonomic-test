package com.jojonomic.jojonomictest;

import com.jojonomic.jojonomictest.entity.Account;
import com.jojonomic.jojonomictest.entity.GoldPrice;
import com.jojonomic.jojonomictest.entity.Transaction;
import com.jojonomic.jojonomictest.repository.AccountRepository;
import com.jojonomic.jojonomictest.repository.GoldPriceRepository;
import com.jojonomic.jojonomictest.repository.TransactionRepository;
import com.jojonomic.jojonomictest.utils.TopUpMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootApplication
public class TopUpStorageApplication {

	private TransactionRepository transactionRepository;

	private AccountRepository accountRepository;

	private GoldPriceRepository goldPriceRepository;

	public TopUpStorageApplication(TransactionRepository transactionRepository, AccountRepository accountRepository, GoldPriceRepository goldPriceRepository) {
		this.transactionRepository = transactionRepository;
		this.accountRepository = accountRepository;
		this.goldPriceRepository = goldPriceRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(TopUpStorageApplication.class, args);
	}

	@Transactional
	@KafkaListener(topics = "top-up", groupId = "e_gold", containerFactory = "topUpListener")
	void onEvent(TopUpMessage message) {
		System.out.println("KAFKAI, INIT");
		Account account = accountRepository.findByAccountNumber(message.getNoRek());

		List<GoldPrice> listGoldPrice = goldPriceRepository.findAll(Sort.by(Sort.Direction.DESC, "createdAt"));

		if (listGoldPrice.size() < 1) {
			return;
		}

		GoldPrice goldPrice = listGoldPrice.get(0);
		Double goldWeight = 0.0;
		Double topUpPrice = 0.0;

		try {
			goldWeight = Double.parseDouble(message.getGram());
			topUpPrice = Double.parseDouble(message.getHarga());
		} catch (Exception e) {
			e.printStackTrace();
		}

		Double currentBalance = account.getBalance() + topUpPrice;

		// Insert transaction
		Transaction transaction = new Transaction();
		transaction.setNoRek(message.getNoRek());
		transaction.setGoldBalance(currentBalance);
		transaction.setGoldWeight(goldWeight);
		transaction.setHargaTopup(topUpPrice);
		transaction.setHargaBuyBack(goldPrice.getBuyBackPrice());
		transaction.setType("topup");
		transaction.setCreatedAt(System.currentTimeMillis());

		transactionRepository.save(transaction);

		// Update balance
		account.setBalance(currentBalance);

		accountRepository.findById(account.getId())
				.map(acc -> {
					acc.setBalance(currentBalance);

					return accountRepository.save(acc);
				});
	}
}
