package com.jojonomic.jojonomictest.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tbl_harga")
public class GoldPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "reff_id")
    private Long reffId;

    @Column(name = "admin_id")
    private String adminId;

    @Column(name = "harga_topup")
    private Double topUpPrice;

    @Column(name = "harga_buyback")
    private Double buyBackPrice;

    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    public GoldPrice() {}

    public GoldPrice(Long reffId, String adminId, Double topUpPrice, Double buyBackPrice, LocalDateTime createdAt) {
        this.reffId = reffId;
        this.adminId = adminId;
        this.topUpPrice = topUpPrice;
        this.buyBackPrice = buyBackPrice;
        this.createdAt = createdAt;
    }

    public Long getReffId() {
        return reffId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Double getTopUpPrice() {
        return topUpPrice;
    }

    public void setTopUpPrice(Double topUpPrice) {
        this.topUpPrice = topUpPrice;
    }

    public Double getBuyBackPrice() {
        return buyBackPrice;
    }

    public void setBuyBackPrice(Double buyBackPrice) {
        this.buyBackPrice = buyBackPrice;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
