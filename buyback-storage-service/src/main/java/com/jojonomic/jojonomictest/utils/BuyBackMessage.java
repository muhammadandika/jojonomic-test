package com.jojonomic.jojonomictest.utils;

public class BuyBackMessage {
    private String gram;

    private String harga;

    private String noRek;

    public BuyBackMessage() {

    }

    public BuyBackMessage(String gram, String harga, String noRek) {
        this.gram = gram;
        this.harga = harga;
        this.noRek = noRek;
    }

    public void setGram(String gram) {
        this.gram = gram;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public void setNoRek(String noRek) {
        this.noRek = noRek;
    }

    public String getGram() {
        return gram;
    }

    public String getHarga() {
        return harga;
    }

    public String getNoRek() {
        return noRek;
    }
}
