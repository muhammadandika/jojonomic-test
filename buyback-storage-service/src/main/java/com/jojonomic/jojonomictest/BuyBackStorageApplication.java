package com.jojonomic.jojonomictest;

import com.jojonomic.jojonomictest.entity.Account;
import com.jojonomic.jojonomictest.entity.GoldPrice;
import com.jojonomic.jojonomictest.entity.Transaction;
import com.jojonomic.jojonomictest.repository.AccountRepository;
import com.jojonomic.jojonomictest.repository.GoldPriceRepository;
import com.jojonomic.jojonomictest.repository.TransactionRepository;
import com.jojonomic.jojonomictest.utils.BuyBackMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootApplication
public class BuyBackStorageApplication {

	private TransactionRepository transactionRepository;

	private AccountRepository accountRepository;

	private GoldPriceRepository goldPriceRepository;

	@Value(value = "${spring.kafka.group.id}")
	static String kafkaGroupId;

	public BuyBackStorageApplication(TransactionRepository transactionRepository, AccountRepository accountRepository, GoldPriceRepository goldPriceRepository) {
		this.transactionRepository = transactionRepository;
		this.accountRepository = accountRepository;
		this.goldPriceRepository = goldPriceRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(BuyBackStorageApplication.class, args);
	}

	@Transactional
	@KafkaListener(topics = "buyback", groupId = "e_gold", containerFactory = "buyBackListener")
	void onEvent(BuyBackMessage message) {
		Account account = accountRepository.findByAccountNumber(message.getNoRek());

		List<GoldPrice> listGoldPrice = goldPriceRepository.findAll(Sort.by(Sort.Direction.DESC, "createdAt"));

		if (listGoldPrice.size() < 1) {
			return;
		}

		GoldPrice goldPrice = listGoldPrice.get(0);
		Double goldWeight = 0.0;
		Double buyBackPrice = 0.0;

		try {
			goldWeight = Double.parseDouble(message.getGram());
			buyBackPrice = Double.parseDouble(message.getHarga());
		} catch (Exception e) {
			e.printStackTrace();
		}

		Double currentBalance = account.getBalance() - buyBackPrice;

		// Insert transaction
		Transaction transaction = new Transaction();
		transaction.setNoRek(message.getNoRek());
		transaction.setGoldBalance(currentBalance);
		transaction.setGoldWeight(goldWeight);
		transaction.setHargaTopup(goldPrice.getTopUpPrice());
		transaction.setHargaBuyBack(buyBackPrice);
		transaction.setType("buyback");
		transaction.setCreatedAt(System.currentTimeMillis());

		transactionRepository.save(transaction);

		// Update balance
		account.setBalance(currentBalance);

		accountRepository.findById(account.getId())
				.map(acc -> {
					acc.setBalance(currentBalance);

					return accountRepository.save(acc);
				});
	}
}
