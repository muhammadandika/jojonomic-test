package com.jojonomic.jojonomictest.requests;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InputGoldPriceRequest {
    @JsonProperty(value = "admin_id")
    String adminId;

    @JsonProperty(value = "harga_topup")
    Double topUpPrice;

    @JsonProperty(value = "harga_buyback")
    Double buyBackPrice;

    @JsonCreator
    public InputGoldPriceRequest() {

    }

    public String getAdminId() {
        return adminId;
    }

    public Double getTopUpPrice() {
        return topUpPrice;
    }

    public Double getBuyBackPrice() {
        return buyBackPrice;
    }
}
