package com.jojonomic.jojonomictest.controller;

import com.jojonomic.jojonomictest.entity.GoldPrice;
import com.jojonomic.jojonomictest.requests.InputGoldPriceRequest;
import com.jojonomic.jojonomictest.responses.SuccessInputPriceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class InputGoldPriceController {
    @Autowired
    private KafkaTemplate<String, GoldPrice> kafkaTemplate;

    @PostMapping("input-harga")
    public ResponseEntity<SuccessInputPriceResponse> inputPrice(@RequestBody InputGoldPriceRequest request) {
        GoldPrice goldPrice = new GoldPrice();
        goldPrice.setAdminId(request.getAdminId());
        goldPrice.setBuyBackPrice(request.getBuyBackPrice());
        goldPrice.setTopUpPrice(request.getTopUpPrice());

        kafkaTemplate.send("input-harga", goldPrice);

        return ResponseEntity.ok(new SuccessInputPriceResponse(goldPrice.getReffId()));
    }
}
