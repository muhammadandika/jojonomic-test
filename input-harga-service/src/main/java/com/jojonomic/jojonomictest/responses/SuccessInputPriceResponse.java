package com.jojonomic.jojonomictest.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SuccessInputPriceResponse {
    @JsonProperty(value = "error")
    private boolean error;

    @JsonProperty(value = "reff_id")
    private Long reffId;

    @JsonCreator
    public SuccessInputPriceResponse(Long reffId) {
        this.error = false;
        this.reffId = reffId;
    }
}
