## E-GOLD

Aplikasi Emas Digital

### Jalankan docker compose sebelum run project ini

```bash
docker-compose -f ./misc/docker-compose.yaml up -d
```

### Tambahkan kode ini di *.rc kalian. ex: .zsrch, .bashrc
```
export G_KAFKA_BOOSTRAP_SERVER={ASK_PROJECT_OWNER}
export G_DB_USERNAME={ASK_PROJECT_OWNER}
export G_DB_PASSWORD={ASK_PROJECT_OWNER}
export G_DB_URL={ASK_PROJECT_OWNER}
```

### Hasil

Link Video:
[Disini](https://drive.google.com/file/d/1aZYiSlJli1jdOerUGBdAEC8PYtVt0sfV/view?usp=sharing)

### POST api/check-harga
![Image 1](./evidence/1.png)

### GET api/check-harga
![Image 2](./evidence/2.png)


### POST api/topup

* Error 1
![Image 3-1](./evidence/3-error-1.png)

* Error 2
![Image 3-2](./evidence/3-error-2.png)

* Success
![Image 3](./evidence/3.png)

### POST api/check-saldo

* Error
![Image 4-1](./evidence/4-error.png)

* Success
![Image 4](./evidence/4.png)

### POST api/mutasi
![Image 5](./evidence/5.png)


### POST api/buyback
![Image 6](./evidence/6.png)

### Kafka
![Kafka](./evidence/kafka.png)