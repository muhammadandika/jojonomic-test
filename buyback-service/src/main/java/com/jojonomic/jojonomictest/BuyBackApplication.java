package com.jojonomic.jojonomictest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuyBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuyBackApplication.class, args);
	}
}
