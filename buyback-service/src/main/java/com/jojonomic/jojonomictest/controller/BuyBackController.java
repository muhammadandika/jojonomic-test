package com.jojonomic.jojonomictest.controller;

import com.jojonomic.jojonomictest.entity.Account;
import com.jojonomic.jojonomictest.repository.AccountRepository;
import com.jojonomic.jojonomictest.requests.BuyBackRequest;
import com.jojonomic.jojonomictest.responses.ErrorBuyBackResponse;
import com.jojonomic.jojonomictest.responses.SuccessBuyBackResponse;
import com.jojonomic.jojonomictest.utils.BuyBackMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class BuyBackController {

    @Autowired
    KafkaTemplate<String, BuyBackMessage> kafkaTemplate;

    @Autowired
    AccountRepository accountRepository;

    @Value(value = "gold.buyback-topic")
    private String buyBackTopic;

    @PostMapping("/buyback")
    public ResponseEntity buyBack(@RequestBody BuyBackRequest request) {
        List<Account> accounts = accountRepository.findAll(Sort.by(Sort.Direction.DESC, "createdAt"));

        if (accounts.size() < 1) {
            return ResponseEntity.ok(new ErrorBuyBackResponse(null, "Akun anda tidak ditemukan"));
        }

        Account account = accounts.get(0);

        Double buyBackPrice = 0.0;

        try {
            buyBackPrice = Double.parseDouble(request.getHarga());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Double remainBalance = account.getBalance() - buyBackPrice;

        if (remainBalance < 0) {
            return ResponseEntity.ok(new ErrorBuyBackResponse(Long.toString(account.getId()), "Saldo anda tidak mencukupi"));
        }

        BuyBackMessage message = new BuyBackMessage();
        message.setGram(request.getGram());
        message.setHarga(request.getHarga());
        message.setNoRek(request.getNoRek());

        kafkaTemplate.send(buyBackTopic, message);

        return ResponseEntity.ok(new SuccessBuyBackResponse(Long.toString(account.getId())));
    }
}
